package model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "rates", schema = "public", catalog = "d3lt7eju0rnvls")
public class RatesEntity {
    private Timestamp datetime;
    private double price;
    private int usersId;
    private long goodsId;

    public RatesEntity() {
    }

    @Basic
    @Column(name = "datetime")
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "price")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "users_id")
    public int getUsersId() {
        return usersId;
    }

    public void setUsersId(int usersId) {
        this.usersId = usersId;
    }

    @Id
    @Column(name = "goods_id")
    public long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(long goodsId) {
        this.goodsId = goodsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RatesEntity that = (RatesEntity) o;

        if (Double.compare(that.price, price) != 0) return false;
        if (usersId != that.usersId) return false;
        if (goodsId != that.goodsId) return false;
        if (datetime != null ? !datetime.equals(that.datetime) : that.datetime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = datetime != null ? datetime.hashCode() : 0;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + usersId;
        result = 31 * result + (int) (goodsId ^ (goodsId >>> 32));
        return result;
    }
}
